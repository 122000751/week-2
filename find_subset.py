def findsubsets(s):
    l = len(s)

    for i in range(1 << l):
        j = 0
        while(i):
            lb = i & 1
            if lb:
                print(s[j], end=" ")
            i = i >> 1
            j += 1
        print()


if __name__ == '__main__':
    s = "abc"
    print(findsubsets(s))

    
