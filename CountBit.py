n = int(input("Enter Number\n"))

def count_bit(n):
    count = 0
    while(n):
        n = n & (n-1)
        count += 1
    return count

if __name__ == '__main__':
    print(count_bit(n))
