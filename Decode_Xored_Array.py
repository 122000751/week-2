from codecs import EncodedFile


def decode_xored(encoded, first):
    arr=[first]
    for i in encoded:
        arr.append(arr[-1]^i)      
    return arr

encoded = [1,2,3]
first = 1
print(decode_xored(encoded, first))