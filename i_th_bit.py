n = int(input("Enter Number\n"))
i = int(input("Enter i\n"))


def get_ith_bit(n, i):
    mask = 1 << i
    ans = n & mask
    if ans:
        return 1

    return 0


if __name__ == '__main__':
    print(get_ith_bit(n, i))
